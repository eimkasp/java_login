import javax.swing.*;
import java.sql.*;
import java.util.Scanner;

public class Main {

    static JFrame loginFrame;
    static JFrame appFrame;


    public static void main(String[] args) {


        showApplicationLogin();

    }



    public static void showApplicationLogin() {
        /* Sukuriame frame objekta ir parodome varotojui */
        loginFrame = new JFrame("Login");
        loginFrame.setContentPane(new Login().panel1);
        loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        loginFrame.pack();
        loginFrame.setVisible(true);
    }

    public static void showApplication() {
        Main.loginFrame.setVisible(false);

        /* Sukuriame frame objekta ir parodome varotojui */
        appFrame = new JFrame("App");
        appFrame.setContentPane(new App().panel1);
        appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        appFrame.pack();
        appFrame.setVisible(true);


    }

}
