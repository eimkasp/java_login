import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Login {
    private JTextField userEmail;
    public JPanel panel1;
    private JLabel userEmailLabel;
    private JPasswordField userPassword;
    private JLabel userPasswordLabel;
    private JButton loginButton;

    public Login() {
        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String inputEmail = userEmail.getText();
                char[] passwordArray = userPassword.getPassword();
                String inputPassword = String.valueOf(passwordArray);
                if (User.checkLoginInformation(inputEmail, inputPassword)) {
                    Main.showApplication();
                } else {
                    JOptionPane.showMessageDialog(panel1, "Login Unsuccessful");
                    userEmail.setText("");
                    userPassword.setText("");
                }
            }
        });
    }


}
