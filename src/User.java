import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class User {

    static User currentUser;

    // JDBC driver name and database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String DB_NAME = "login";
    static final String DB_URL = "jdbc:mysql://localhost/" + DB_NAME + "?useSSL=true";

    //  Database credentials
    static final String USER = "root";
    static final String PASS = "root";

    private int id;
    private String email;

    private String name;
    private String password;
    private String lastLoginDate;


    public User(int id, String email, String password) {
        this.id = id;
        this.email = email;
        this.password = password;
    }


    public User(int id, String email, String name, String password) {
        this.id = id;
        this.email = email;
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static boolean checkLoginInformation(String inputEmail, String inputPassword) {
        Connection connection = null;
        PreparedStatement stmt = null;
        boolean isLoggedIn = false;

        inputPassword = User.hashPassword(inputPassword);

        try {
            //STEP 2: Register JDBC driver
            Class.forName("com.mysql.jdbc.Driver");

            //STEP 3: Open a connection
            System.out.println("Connecting to database...");
            connection = DriverManager.getConnection(DB_URL, USER, PASS);



            //STEP 4: Execute a query
            System.out.println("Creating statement...");
            String sql;
            sql = "SELECT * FROM users WHERE email = ? AND password = ?";
            stmt = connection.prepareStatement(sql);
            stmt.setString(1, inputEmail);
            stmt.setString(2, inputPassword);

            ResultSet rs = stmt.executeQuery();


            //STEP 5: Extract data from result set
            while (rs.next()) {
                //Retrieve by column name
                int id = rs.getInt("id");
                String  email = rs.getString("email");
                String  name = rs.getString("name");
                String  password = rs.getString("password");

                User.currentUser = new User(id, email,name,  password);
                isLoggedIn = true;
            }

            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            connection.close();

            return isLoggedIn;

        } catch(Exception e){
            // Sita programos dalis veiks jei kazkas ivyks blogai try dalyje
            e.printStackTrace();
            return false;
        }
    }

    public static String hashPassword(String password) {

        try {
            String stringToHash = password;
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(stringToHash.getBytes());
            byte[] digiest = messageDigest.digest();
            String hashedOutput = DatatypeConverter.printHexBinary(digiest);
            System.out.println(hashedOutput);

            return hashedOutput;
        } catch (Exception e) {
            return null;
        }
    }
}
